import React from "react";
import './transmitterSettings.css'


export default function TransmitterSettings() 
{
    const [radius, setRadius] = React.useState(JSON.parse(localStorage.getItem('transmitter')).radius);

    const saveTransmitterSettings = (e) => 
    {
        e.preventDefault();
        const formData = new FormData(e.target);
        const transmitterSettings = Object.fromEntries(formData.entries());
        localStorage.setItem('transmitter', JSON.stringify(transmitterSettings));
    }

    const setTransmitterRadius = (e) => {
        setRadius(e.target.value)
    }
  
    return (
        <form onSubmit={saveTransmitterSettings}>
            <input placeholder="Radius, m" onChange={setTransmitterRadius} name="radius" value={radius}/>
            <button className='transmitter-save-settings-btn' type="submit">Save settings</button>
        </form>
    );
  }